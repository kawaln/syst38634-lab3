package password;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	/*
	 * @BeforeClass public static void setUpBeforeClass() throws Exception { }
	 * 
	 * @AfterClass public static void tearDownAfterClass() throws Exception { }
	 * 
	 * @Before public void setUp() throws Exception { }
	 * 
	 * @After public void tearDown() throws Exception { }
	 */

	@Test
	public void checkPasswordDigits() {
		boolean password = PasswordValidator.checkPasswordDigits("Test12");
		System.out.println(password);
		assertTrue("Valid password Digit", password == true);

	}

	@Test
	public void checkPasswordDigitsException() {
		boolean password = PasswordValidator.checkPasswordDigits("abcde");

		assertTrue("Invalid password Digit", password == false);

	}

	@Test
	public void testCheckPasswordDigitsBoundaryIn() {
		boolean password = PasswordValidator.checkPasswordDigits("abcdefg12");
		assertTrue("password is ok for digits", password == true);
	}

	@Test
	public void testCheckPasswordDigitsBoundaryOut() {
		boolean password = PasswordValidator.checkPasswordDigits("abcdefghijr1");
		assertTrue("password is weak on digits", password == false);
	}

	@Test
	public void testCheckPasswordLength() {
		boolean password = PasswordValidator.checkPasswordLength("123456789");
		System.out.println(password);
		assertTrue("Valid password length ", password == true);

	}

	@Test
	public void testCheckPasswordLengthException() {
		boolean password = PasswordValidator.checkPasswordLength("abc123");
		assertTrue("Invalid password length ", password == false);

	}

	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		boolean password = PasswordValidator.checkPasswordLength("12345678");
		System.out.println(password);
		assertTrue("password length is fine", password == true);
	}

	@Test
	public void testCheckPasswordLengthBoudaryOut() {
		boolean password = PasswordValidator.checkPasswordLength("1234567");
		assertTrue("password length is short", password == false);
	}

}
